/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/App.vue',
    './src/shared/*.{html,js,vue}',
    './src/app/views/**/*.{html,js,vue}',
    './src/app/components/**/*.{html,js,vue}',
    './src/auth/views/**/*.{html,js,vue}',
    './src/auth/components/**/*.{html,js,vue}',
  ],
  theme: {
    extend: {
      colors:{
        "button-color":"#2F80ED",
        "hover-button":"#0353a4"
      },
      backgroundImage: {
        'sectionBanner': "url('/images/bannerSection.jpg')",
      }
    },
    fontFamily: {
      workSans:['Work Sans', 'sans-serif']
    },
    container:{
      width:"1240px",
      padding:"127.25px",
      center:true
    }
  },
  plugins: [],
}

