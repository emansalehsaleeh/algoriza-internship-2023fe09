import {defineStore} from 'pinia'
import axios from 'axios'
export const useSearchStore = defineStore('search',{

    state: () => ({
        listHotels: [],
        Loading:false,
        error :''
    }),
    actions:{
        async getListHotel( destId , checkIn , checkOut , adults , roomNum  , priceMin=null ,priceMax=null, sortId=null ){
            this.Loading = true

            try{

                const options = {

                        method: 'GET',
                        url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels',
                        params: {
                            dest_id: destId,
                            search_type: 'CITY',
                            arrival_date: checkIn,
                            departure_date: checkOut,
                            adults: adults,
                            room_qty: roomNum,
                            price_min: priceMin,
                            price_max: priceMax,
                            sort_by: sortId,
                            page_number: '1'
                        },
                        headers: {
                            'X-RapidAPI-Key': 'a809b06adbmsh456c870f5180c2cp140332jsne8490a895b87',
                            'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
                        }
                };
    
                const response = await axios.request(options);
                this.listHotels = response.data.data.hotels

            }catch(error){
                this.error = 'An error occurred while fetching data.';
            }finally{
                this.Loading = false
            }


        }
    }

})