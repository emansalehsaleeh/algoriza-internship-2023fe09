import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '../../auth/views/LoginView.vue'
import Registration from '../../auth/views/RegistrationView.vue'
import SeachResult from '../views/SearchResultView.vue'
import HotelDetails from '../views/HotelDetailsView.vue'
import Trips from '../views/TripsView.vue'
import NotFound from '../views/NotFoundView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/register',
      name: 'register',
      component: Registration
    },
    {
      path: '/searchResult',
      name: 'searchResult',
      component: SeachResult,
    },
    {
      path: '/hotelDetails',
      name: 'hotelDetails',
      component: HotelDetails,
    },
    {
      path: '/trips',
      name: 'trips',
      component: Trips,
    },
    {
      path:'/:catchAll(.*)',
      name: 'notFound',
      component: NotFound
    }
  ]
  
})

export default router
